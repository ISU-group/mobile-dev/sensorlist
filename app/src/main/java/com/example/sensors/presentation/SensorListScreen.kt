package com.example.sensors.presentation

import android.hardware.Sensor
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun SensorListScreen(sensors: List<Sensor>) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(sensors) { item: Sensor ->
            Text(
                text = item.stringType,
            )
        }
    }
}