package com.example.sensors

import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.sensors.ui.theme.SensorsTheme
import java.util.*

class MainActivity : ComponentActivity() {
    private lateinit var sensorManager: SensorManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        val sensors = getAllSensors().mapKeys {
            when (it.key) {
                SensorType.MOTION -> getString(R.string.sensor_motion)
                SensorType.ENVIRONMENT -> getString(R.string.sensor_environment)
                SensorType.POSITION -> getString(R.string.sensor_position)
            }
        }

        setContent {
            SensorsTheme {
                MainScreen(sensors = sensors)
            }
        }
    }

    private fun getAllSensors(): Map<SensorType, List<Sensor>> {
        val sensors = mutableMapOf<SensorType, List<Sensor>>()

        for (sensorType in SensorType.values()) {
            sensors[sensorType] = when (sensorType) {
                SensorType.POSITION -> getPositionSensors()
                SensorType.ENVIRONMENT -> getEnvironmentSensors()
                SensorType.MOTION -> getMotionSensors()
            }
        }

        return sensors.toMap()
    }

    private fun getMotionSensors() = getSensors(MOTION_SENSORS)

    private fun getPositionSensors() = getSensors(POSITION_SENSORS)

    private fun getEnvironmentSensors() = getSensors(ENVIRONMENT_SENSORS)

    private fun getSensors(types: Array<Int>): List<Sensor> {
        val sensors = mutableListOf<Sensor>()

        for (sensorType in types) {
            val sensorList = sensorManager.getSensorList(sensorType)
            if (sensorList.isNotEmpty()) {
                sensors.addAll(sensorList)
            }
        }

        return sensors.toList()
    }
}

//@Composable
//fun SensorsList(sensors: Map<SensorType, List<Sensor>>) {
//    LazyColumn() {
//        items(sensors.size) { index ->
//            Text(
//                text = "${sensors[index].name} | ${sensors[index]} | ${sensors[index].power} | ${sensors[index].type}",
//                overflow = TextOverflow.Clip
//            )
//        }
//
//        items(sensors.entries.toList()) { (sensorType, sensors) ->
//            Text
//        }
//    }
//}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    SensorsTheme {
        Greeting("Android")
    }
}