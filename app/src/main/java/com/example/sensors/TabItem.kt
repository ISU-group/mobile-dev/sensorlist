package com.example.sensors

import android.hardware.Sensor
import androidx.compose.runtime.Composable
import com.example.sensors.presentation.*

typealias ComposableFun = @Composable () -> Unit

data class TabItem(var title: String, var sensors: List<Sensor>, var screen: ComposableFun) {}
