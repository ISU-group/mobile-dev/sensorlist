package com.example.sensors

import android.hardware.Sensor
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.sensors.components.Tabs
import com.example.sensors.components.TabsContent
import com.example.sensors.components.TopBar
import com.example.sensors.presentation.SensorListScreen
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.rememberPagerState

@OptIn(ExperimentalPagerApi::class)
@Composable
fun MainScreen(
    sensors: Map<String, List<Sensor>>
) {
    val tabs = mutableListOf<TabItem>()
    for ((k, v) in sensors) {
        tabs.add(TabItem(k, v) { SensorListScreen(v) })
    }

    val pagerState = rememberPagerState(initialPage = 0)

    Scaffold(
        topBar = { TopBar() },
    ) {
        Column {
            Tabs(tabs = tabs, pagerState = pagerState)
            TabsContent(tabs = tabs, pagerState = pagerState)
        }
    }
}